import { IActivityTypes } from './activityTypes';

export class ApplicationState {
  public activityTypes: IActivityTypes;
}
